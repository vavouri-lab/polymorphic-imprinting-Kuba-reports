// Your CSS as text
var styles = `
.pw_prompt {
    all: inherit;
    margin:  10vh auto;
    background-color: white;

    padding:15px;
    width:400px;

    border:1px solid black;
}
.pw_prompt label {
    text-align: center;
    display:block; 
    margin-bottom:10px;
}
.pw_prompt input {
    width: 350px;
    margin-bottom:10px;
}

.pw_prompt button{
  position: relative;
  top: 5px;
  left: 20%;
  width: 200px;
}
`;




var styleSheet = document.createElement("style");
styleSheet.type = "text/css";
styleSheet.innerText = styles;
document.head.appendChild(styleSheet);



var promptCount = 0;
window.pw_prompt = function(options) {
    var lm = options.lm || "Password:",
        bm = options.bm || "Submit";
    if(!options.callback) { 
        alert("No callback function provided! Please provide one.") 
    };

    var prompt = document.createElement("div");
    prompt.className = "pw_prompt";

    var submit = function() {
        options.callback(input.value);
        document.body.removeChild(prompt);
    };

    var label = document.createElement("label");
    label.textContent = lm;
    label.for = "pw_prompt_input" + (++promptCount);
    prompt.appendChild(label);

    var input = document.createElement("input");
    input.id = "pw_prompt_input" + (promptCount);
    input.type = "password";
    input.addEventListener("keyup", function(e) {
        if (e.keyCode == 13) submit();
    }, false);
    prompt.appendChild(input);

    var button = document.createElement("button");
    button.textContent = bm;
    button.addEventListener("click", submit, false);
    prompt.appendChild(button);

    document.body.appendChild(prompt);
};

pw_prompt({
    lm:"Please enter your password:", 
    callback: function(password) {
        if (password != "ijc"){
            alert("Wrong password");
            window.location.reload();
        } else {
            document.getElementById("contents").style.display = "block";
        }



    }
});